import {
  stopForwarding,
  receiveState as defaultReceiveState,
  validateForwardAction,
  getDefaultOptions
} from './utils';
import { actionTypes } from './utils/types';

export * from './utils';

export const createStateSyncMiddleware = (params = {}) => {
  const options = Object.assign({}, getDefaultOptions(), params);
  const { subscribeAction, InitializeSyncEnhancer, initState, syncInitState, forwardAction } =
    options;
  return store => {
    subscribeAction(store, action => store.dispatch(stopForwarding(action)));
    InitializeSyncEnhancer(store);
    if (initState) syncInitState(store);
    return next => action => {
      if (validateForwardAction(action, options)) forwardAction(action);
      return next(action);
    };
  };
};

export const createInitStateSync =
  (rootReducer, receiveState = defaultReceiveState) =>
  (state, action) => {
    switch (action.type) {
      case actionTypes.RECEIVE_INIT_STATE:
        return rootReducer(receiveState(state, action.payload), action);
      default:
        return rootReducer(state, action);
    }
  };
