import { ipcMain, webContents } from 'electron';
import { stopForwarding, validateForwardAction, getDefaultOptions } from './utils';
import { ipcEvents } from './utils/types';

const forwardAction = (action, sendId) => {
  // Forward it to all of the other renderers
  webContents.getAllWebContents().forEach(contents => {
    // Ignore the renderer that sent the action and chromium devtools
    if (contents.id !== sendId && !contents.getURL().startsWith('devtools://')) {
      contents.send(ipcEvents.ACTION, action);
    }
  });
};

export const createStateSyncMiddleware = (params = {}) => {
  const options = Object.assign({}, getDefaultOptions(), params);
  return store => {
    ipcMain.on(ipcEvents.ACTION, (event, action) => {
      store.dispatch(stopForwarding(action));
      forwardAction(action, event.sender.id);
    });

    ipcMain.handle(ipcEvents.GET_INIT_STATE, event => {
      return JSON.stringify(store.getState());
    });

    return next => action => {
      if (validateForwardAction(action, options)) forwardAction(action);
      return next(action);
    };
  };
};
