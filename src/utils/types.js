const ipcPrefix = 'redux-state-sync-ipc.';

export const ipcEvents = {
  ACTION: `${ipcPrefix}ACTION`,
  GET_INIT_STATE: `${ipcPrefix}GET_INIT_STATE`,
  RECEIVE_INIT_STATE: `${ipcPrefix}RECEIVE_INIT_STATE`
};

const actionPrefix = 'redux-state-sync.';

export const actionTypes = {
  GET_INIT_STATE: `${actionPrefix}GET_INIT_STATE`,
  SEND_INIT_STATE: `${actionPrefix}SEND_INIT_STATE`,
  RECEIVE_INIT_STATE: `${actionPrefix}RECEIVE_INIT_STATE`
};
