import { ipcRenderer } from 'electron';
import { receiveIniteState } from './actions';
import { ipcEvents } from './types';

export const subscribeAction = (store, callback) => {
  ipcRenderer.on(ipcEvents.ACTION, (event, action) => {
    callback(action);
  });
};

export const forwardAction = action => {
  ipcRenderer.send(ipcEvents.ACTION, action);
};

export const InitializeSyncEnhancer = store => {
  ipcRenderer.on(ipcEvents.GET_INIT_STATE, _event => {
    ipcRenderer.send(ipcEvents.RECEIVE_INIT_STATE, JSON.stringify(store.getState()));
  });
};

export const syncInitState = store => {
  ipcRenderer.invoke(ipcEvents.GET_INIT_STATE).then(result => {
    if (!result) return;
    store.dispatch(receiveIniteState(JSON.parse(result)));
  });
};
