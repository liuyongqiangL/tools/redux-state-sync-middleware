import isPlainObject from 'lodash.isplainobject';
import isString from 'lodash.isstring';

export const customElectronApiKey = '__electron-redux-state-sync.preload';

export const isLocal = action => action?.meta?.scope === 'local';

const isValidKey = key => ['type', 'payload', 'error', 'meta'].indexOf(key) > -1;

// 参考 https://github.com/redux-utilities/flux-standard-action ，将 isValidKey 限制修改为可选
export const isFSA = (action, checkValidKey = true) =>
  isPlainObject(action) &&
  isString(action.type) &&
  (!checkValidKey || Object.keys(action).every(isValidKey));

export const stopForwarding = action => ({
  ...action,
  meta: {
    ...action.meta,
    scope: 'local'
  }
});

export const validateForwardAction = (
  action,
  { blacklist, blacklistRegular, checkValidKey } = {}
) => {
  return (
    isFSA(action, checkValidKey) &&
    !isLocal(action) &&
    !blacklist.includes(action.type) &&
    blacklistRegular.every(rule => !rule.test(action.type))
  );
};

const subscribeAction = (store, callback) => {};

const forwardAction = (action, options) => {};

const InitializeSyncEnhancer = store => {};

const syncInitState = store => {};

export const receiveState = (prevState, nextState) => nextState;

export const mergeReceiveStore = (prevState, nextState = {}) => {
  return Object.keys(prevState).reduce((previousValue, key) => {
    previousValue[key] = nextState.hasOwnProperty(key) ? nextState[key] : prevState[key];
    return previousValue;
  }, {});
};

export const getDefaultOptions = () => {
  const customOptions = typeof window === 'object' && window?.[customElectronApiKey];
  return Object.assign(
    {},
    {
      checkValidKey: true,
      subscribeAction,
      forwardAction,
      InitializeSyncEnhancer,
      blacklist: [],
      blacklistRegular: [/^!/, /^redux-form/],
      initState: true,
      syncInitState
    },
    customOptions
  );
};
