import { stopForwarding } from './index';
import { actionTypes } from './types';

export const receiveIniteState = state =>
  stopForwarding({
    type: actionTypes.RECEIVE_INIT_STATE,
    payload: state
  });
