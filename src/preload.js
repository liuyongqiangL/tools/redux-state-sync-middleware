import { contextBridge } from 'electron';
import { customElectronApiKey } from './utils';
import * as preload from './utils/renderer';

try {
  contextBridge.exposeInMainWorld(customElectronApiKey, preload);
} catch {
  window[customElectronApiKey] = preload;
}

export default preload;
