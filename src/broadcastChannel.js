import { BroadcastChannel } from 'broadcast-channel';
import { receiveIniteState } from './utils/actions';
import { ipcEvents } from './utils/types';

const defaultActionOptions = {
  channel: 'redux-state-sync-action-channel'
};

const defaultInitStateOptions = {
  channel: 'redux-state-sync-init-channel'
};

export class StateSyncChannel {
  constructor(actionOptions, initStateOptions) {
    const { channel: actionChannelName, options: actionChannelOptions } = actionOptions;
    const { channel: initStateChannelName, options: initStateChannelOptions } = initStateOptions;
    this.actionChannel = new BroadcastChannel(actionChannelName, actionChannelOptions);
    this.initStateChannel = new BroadcastChannel(initStateChannelName, initStateChannelOptions);
  }

  subscribeAction = (store, callback) => {
    this.actionChannel.onmessage = action => callback(action);
  };

  forwardAction = action => {
    this.actionChannel.postMessage(action);
  };

  InitializeSyncEnhancer = store => {
    this.initStateChannel.onmessage = msg => {
      const { channelCmd, data } = msg;
      switch (channelCmd) {
        case ipcEvents.GET_INIT_STATE:
          this.initStateChannel.postMessage({
            channelCmd: ipcEvents.RECEIVE_INIT_STATE,
            data: store.getState()
          });
          break;
        case ipcEvents.RECEIVE_INIT_STATE:
          store.dispatch(receiveIniteState(data));
          break;
        default:
          break;
      }
    };
  };

  syncInitState = store => {
    this.initStateChannel.postMessage({ channelCmd: ipcEvents.GET_INIT_STATE });
  };

  getOptions = () => {
    return {
      subscribeAction: this.subscribeAction,
      forwardAction: this.forwardAction,
      InitializeSyncEnhancer: this.InitializeSyncEnhancer,
      syncInitState: this.syncInitState
    };
  };
}

export const creatStateSyncChannel = (actionOptions, initStateOptions) => {
  const stateSyncChannel = new StateSyncChannel(
    Object.assign({}, defaultActionOptions, actionOptions),
    Object.assign({}, defaultInitStateOptions, initStateOptions)
  );
  return stateSyncChannel.getOptions();
};
