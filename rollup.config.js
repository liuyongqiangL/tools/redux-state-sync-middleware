const babel = require('rollup-plugin-babel');
const commonjs = require('@rollup/plugin-commonjs');
const pkg = require('./package.json');

const extensions = ['.js'];

const basePlugins = [commonjs()];

const baseConfig = {
  external: Object.keys(pkg.peerDependencies || {}),
  plugins: [
    ...basePlugins,
    babel({
      extensions
    })
  ]
};

module.exports = [
  {
    ...baseConfig,
    input: ['src/index.js', 'src/main.js', 'src/preload.js', 'src/broadcastChannel.js'],
    output: [
      { dir: 'lib', format: 'cjs' },
      { dir: 'es', format: 'es' }
    ]
  }
];
