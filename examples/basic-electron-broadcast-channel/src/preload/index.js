import { contextBridge, ipcRenderer } from 'electron';
import { customElectronApiKey } from 'redux-state-sync-middleware/src/index';
import { creatStateSyncChannel } from 'redux-state-sync-middleware/src/broadcastChannel';

const electronPreload = {
  new_window: () => {
    ipcRenderer.send('WINDOW_NEW');
  }
};

export const preload = () => {
  const options = creatStateSyncChannel();
  try {
    contextBridge.exposeInMainWorld(customElectronApiKey, options);
    contextBridge.exposeInMainWorld('electronPreload', electronPreload);
  } catch {
    window.electronPreload = electronPreload;
    window[customElectronApiKey] = options;
  }
};

preload();
