import { createStore, applyMiddleware } from 'redux';
import {
  createStateSyncMiddleware,
  createInitStateSync,
  mergeReceiveStore
} from 'redux-state-sync-middleware/src/index';
import { generateRandomJson } from '@examples/common';
import homeReducers from '@examples/common/store/reducers/home';
import chileReducers from '@examples/common/store/reducers/chile';
import {
  commTest,
  decrementGlobalCounter,
  incrementGlobalCounter,
  incrementLocalCounter,
  decrementLocalCounter,
  chileIncrementLocalCounter,
  chileDecrementLocalCounter
} from '@examples/common/store/actions';

document.getElementById('new_window').addEventListener('click', window.electronPreload.new_window);

const isChild = Boolean(location.search);
const rootReducer = isChild ? chileReducers : homeReducers;
const store = createStore(
  createInitStateSync(rootReducer, mergeReceiveStore),
  applyMiddleware(createStateSyncMiddleware())
);

console.log('store', rootReducer, store);
const registerClickHandler = (id, handler) =>
  document.getElementById(id)?.addEventListener('click', () => {
    console.log('click time', Date.now());
    handler();
  });

const registerEvents = () => {
  registerClickHandler('incrementGlobalCounter', () => store.dispatch(incrementGlobalCounter()));
  registerClickHandler('decrementGlobalCounter', () => store.dispatch(decrementGlobalCounter()));
  if (isChild) {
    registerClickHandler('incrementLocalCounter', () =>
      store.dispatch(chileIncrementLocalCounter())
    );
    registerClickHandler('decrementLocalCounter', () =>
      store.dispatch(chileDecrementLocalCounter())
    );
  } else {
    registerClickHandler('incrementLocalCounter', () => store.dispatch(incrementLocalCounter()));
    registerClickHandler('decrementLocalCounter', () => store.dispatch(decrementLocalCounter()));
  }
  registerClickHandler('test-comm', () => {
    const data = generateRandomJson(10 * 1024);
    return store.dispatch(commTest(data));
  });
};

const subscribeRender = () => {
  const globalCounterEl = document.getElementById('globalCounter');
  const localCounterEl = document.getElementById('localCounter');

  const render = () => {
    const { globalCounter, localCounter, chileLocalCounter } = store.getState();
    console.log(Date.now(), store.getState());
    globalCounterEl.innerHTML = globalCounter.toString();
    localCounterEl.innerHTML = `localCounter:${
      localCounter !== undefined ? localCounter.toString() : 'undefined'
    }  chileLocalCounter:${
      chileLocalCounter !== undefined ? chileLocalCounter.toString() : 'undefined'
    }`;
  };

  render();
  store.subscribe(render);
};

registerEvents();
subscribeRender();
