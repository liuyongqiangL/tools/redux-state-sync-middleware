const base = require('./webpack.config');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  ...base,
  mode: 'development',
  entry: {
    renderer: './src/renderer/index.js'
  },
  target: 'web',
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/renderer/index.html',
      publicPath: './'
    })
  ]
};
