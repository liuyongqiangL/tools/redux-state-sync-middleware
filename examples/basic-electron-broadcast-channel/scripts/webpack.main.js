const base = require('./webpack.config');

module.exports = {
  ...base,
  mode: 'development',
  entry: {
    main: './src/main/index.js'
  },
  target: 'electron-main',
  node: {
    __dirname: false
  }
};
