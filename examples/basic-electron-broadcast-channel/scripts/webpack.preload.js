const base = require('./webpack.config');

module.exports = {
  ...base,
  mode: 'development',
  entry: {
    preload: './src/preload/index.js'
  },
  target: 'electron-renderer'
};
