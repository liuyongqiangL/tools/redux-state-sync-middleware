import {
  COMM_TEST,
  INCREMENT_GLOBAL,
  DECREMENT_GLOBAL,
  INCREMENT_LOCAL,
  DECREMENT_LOCAL,
  CHILE_INCREMENT_LOCAL,
  CHILE_DECREMENT_LOCAL
} from './type';

export const commTest = testData => ({ type: COMM_TEST, meta: { data: testData } });

export const incrementGlobalCounter = () => ({ type: INCREMENT_GLOBAL });

export const decrementGlobalCounter = () => ({ type: DECREMENT_GLOBAL });

export const incrementLocalCounter = () => ({ type: INCREMENT_LOCAL });

export const decrementLocalCounter = () => ({ type: DECREMENT_LOCAL });

export const chileIncrementLocalCounter = () => ({ type: CHILE_INCREMENT_LOCAL });

export const chileDecrementLocalCounter = () => ({ type: CHILE_DECREMENT_LOCAL });
