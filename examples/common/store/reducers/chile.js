import { combineReducers } from 'redux';
import commonReducers from './commonReducers';
import { chileLocalCounter } from './reducers';

export default combineReducers({
  ...commonReducers,
  chileLocalCounter
});
