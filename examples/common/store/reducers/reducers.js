import {
  COMM_TEST,
  DECREMENT_GLOBAL,
  INCREMENT_GLOBAL,
  INCREMENT_LOCAL,
  DECREMENT_LOCAL,
  CHILE_INCREMENT_LOCAL,
  CHILE_DECREMENT_LOCAL
} from '../type';

export const commonConfig = (state = {}, action) => {
  switch (action.type) {
    case COMM_TEST: {
      return { ...state, data: action?.meta?.data };
    }
    default:
      return state;
  }
};

export const globalCounter = (state = 0, action) => {
  switch (action.type) {
    case INCREMENT_GLOBAL: {
      return state + 1;
    }
    case DECREMENT_GLOBAL: {
      return state - 1;
    }
    default:
      return state;
  }
};

export const localCounter = (state = 0, action) => {
  switch (action.type) {
    case INCREMENT_LOCAL: {
      return state + 1;
    }
    case DECREMENT_LOCAL: {
      return state - 1;
    }
    default:
      return state;
  }
};

export const chileLocalCounter = (state = 0, action) => {
  switch (action.type) {
    case CHILE_INCREMENT_LOCAL: {
      return state + 1;
    }
    case CHILE_DECREMENT_LOCAL: {
      return state - 1;
    }
    default:
      return state;
  }
};
