import { commonConfig, globalCounter } from './reducers';

export default {
  commonConfig,
  globalCounter
};
