import { combineReducers } from 'redux';
import { localCounter } from './reducers';
import commonReducers from './commonReducers';

export default combineReducers({
  ...commonReducers,
  localCounter
});
