import { JSONSchemaFaker } from 'json-schema-faker';

export const generateRandomJson = (size = 1 * 1024) => {
  const itemsLength = Math.ceil(Math.sqrt(size));
  const items = {
    id: 'items',
    type: 'object',
    user: {
      id: 'user',
      type: 'object',
      properties: {
        id: {
          type: 'integer',
          minimum: 0,
          exclusiveMinimum: true
        },
        value: {
          type: 'string',
          minLength: 1024 - 20,
          maxLength: 1024
        }
      },
      required: ['id', 'value']
    },
    properties: {
      array: {
        type: 'array',
        items: { $ref: 'user' },
        minItems: itemsLength,
        maxItems: itemsLength
      }
    },
    required: ['array']
  };

  const schema = {
    type: 'object',
    items,
    properties: {
      list: {
        type: 'array',
        items: { $ref: 'items' },
        minItems: itemsLength,
        maxItems: itemsLength
      }
    },
    required: ['list']
  };

  console.time('generateTime');
  const data = JSONSchemaFaker.generate(schema);
  console.timeEnd('generateTime');
  console.log('Generate json end', {
    currentTime: Date.now(),
    size: `${JSON.stringify(data).length / 1024 / 1024}M`
  });
  return data;
};
