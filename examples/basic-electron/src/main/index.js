import path from 'path';
import { app, BrowserWindow, ipcMain } from 'electron';
import { createStateSyncMiddleware } from 'redux-state-sync-middleware/src/main';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from '@examples/common/store/reducers/home';

const TESTING = process.env.SPECTRON === 'true';
let mainWindow;

async function createBrowserWindow(browserWindow = null, main = false) {
  // Create the browser window.
  browserWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: `${__dirname}/preload.js`,
      contextIsolation: !TESTING,
      nodeIntegration: TESTING
    }
  });
  browserWindow.webContents.on('did-finish-load', () => {
    browserWindow.webContents.openDevTools();
  });

  await browserWindow.loadURL(
    `file://${path.join(__dirname, './index.html')}${main ? '' : '?child=1'}`
  );

  browserWindow.on('closed', () => {
    browserWindow = null;
  });
}

async function createWindow() {
  await createBrowserWindow(mainWindow, true);
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) createWindow();
});

ipcMain.on('WINDOW_NEW', () => {
  createBrowserWindow();
});

createStore(rootReducer, applyMiddleware(createStateSyncMiddleware()));
