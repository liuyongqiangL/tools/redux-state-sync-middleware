import { contextBridge, ipcRenderer } from 'electron';
import 'redux-state-sync-middleware/src/preload';

const electronPreload = {
  new_window: () => {
    ipcRenderer.send('WINDOW_NEW');
  }
};

export const preload = () => {
  try {
    contextBridge.exposeInMainWorld('electronPreload', electronPreload);
  } catch {
    window.electronPreload = electronPreload;
  }
};

preload();
