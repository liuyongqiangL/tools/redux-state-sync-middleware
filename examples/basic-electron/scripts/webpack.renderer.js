const base = require('./webpack.config');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  ...base,
  mode: 'development',
  entry: {
    preload: './src/preload/index.js',
    renderer: './src/renderer/index.js'
  },
  target: 'electron-renderer',
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/renderer/index.html',
      publicPath: './'
    })
  ]
};
